#!/bin/bash 
python3 /appz/scripts/k8s-v2/start.py 
if [ $? -ne 0 ]; then 
	exit 0 
fi
ansible-playbook /appz/scripts/k8s-v2/main.yaml  -i /appz/scripts/k8s-v2/inventory.txt
