import os
import subprocess
ips = str(os.environ['NODE_IP'])
node_ips = ips.split(",")
ansible_user=str(os.environ['USER_NAME'])
ansible_key=str(os.environ['KEY_NAME'])
print(node_ips)
count=len(node_ips)
for i in range(count):
    node_no=i+1
    host=node_ips[i]
    with open ('/appz/scripts/k8s-v2/inventory.txt','a+') as file:
         contents = file.read()
         if host not in contents:
             file.write("node-%s ansible_host=%s ansible_user=%s ansible_ssh_private_key_file=%s\n"%(node_no,host,ansible_user,ansible_key))
             file.close()
#subprocess.call(['/appz/scripts/k8s-v2/start.sh'])
